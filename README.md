# My personal startpages

## Previews:

### White Garden
![alt text](images/white_garden.png)
### Sea Waves
![alt text](images/sea_waves.png)
### Retro Fitness
![alt text](images/retro_fitness.png)
### Dark Flowers
![alt text](images/dark_flowers.png)
### Desert Shades
![alt text](images/desert_shades.png)

## Credits:
```
+ JQuery
+ FontAwesome
+ Whatever sites I got the fonts from
+ Whoever made the bg images
+ Wherever I got the ping script from
```

## Todo:
```
+ Favicon for shades
+ Color tweaks/adjustments
+ Account for Y-Overflow and use a scroll bar for ".expanded-menu"
	+ The "white-space: nowrap;" in ".expanded-menu" helps with additional entries
	+ Style the scroll bar
		+ Have bar the same along the white line that is the border above
		+ Along the white line, a pink or white box to slide
	+ Ensure that there is a smooth transition
+ Overflow within the sub-menu
	+ Normally goes down
	+ Overflow should go to the right
		+ Really hope I don't need to make more <div>s
```
