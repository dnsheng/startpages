function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
	var month = getMonthStr(today.getMonth());
	var d = today.getDate();
	var dow = getDoW(today.getDay());
	var am_pm;
    m = checkTime(m);
    s = checkTime(s);
	setMessage(h, m);
	if (h >= 12) {
		h = h - 12;
		am_pm = "午後";
	} else {
		am_pm = "午前";
	}
    document.getElementById('clock').innerHTML =
	month + " " + d + "日（" + dow + "）" + h + ":" + m + ":" + s + " " + am_pm;
    var t = setTimeout(startTime, 500);
}

function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function getMonthStr(m) {
	m++;
	return m + "月";
}

function getDoW(d) {
	switch(d) {
		case 0:
			return "日";
		case 1:
			return "月";
		case 2:
			return "火";
		case 3:
			return "水";
		case 4:
			return "木";
		case 5:
			return "金";
		case 6:
			return "土";
	}
}
	
function setMessage(h, m) {
	var message = document.getElementById('message').innerHTML;
	if (h > 5 && h <= 11) {
		document.getElementById('message').innerHTML = "おはいよう";
	} else if (h > 11 && h <= 16) {
		document.getElementById('message').innerHTML = "こんにちは";
	} else if (h > 16 && h <= 22) {
		document.getElementById('message').innerHTML = "こんばんは";
	} else if (h > 22 || h <= 2) {
		document.getElementById('message').innerHTML = "お疲れ様";
	} else if (h > 2 && h <= 5) {
		document.getElementById('message').innerHTML = "大変忙しい";
	}
}
