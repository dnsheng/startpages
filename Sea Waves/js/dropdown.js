$(document).ready(function() {

	/* Toggles status of class "content", changes chevron icon for each state */
	var current_menu, current_select;
	var same_open;

	$('body').click(function(e) {
		var action = e.target;
		var myClass = action.classList.item(0);
		if (myClass == "main-section") {
			// The dropdown was clicked
			var selected_id = action.id;
			var menu_id = selected_id + "-menu";

			// Variables for:
				// + The dropdown (this_menu)
				// + The selector (this_select)
			var this_menu = $('#' + menu_id);
			var this_select = $('#' + selected_id);

			if (current_menu == undefined) {
				/* Page just loaded, first click */
				// Slide down and do action
				this_menu.slideDown(300);
				this_select.addClass("selected");
				// Remember status
				current_menu = this_menu;
				current_select = this_select;
				same_open = 1;
			} else if (current_menu.attr('id') == this_menu.attr('id')) {
				/* Clicking previously clicked */
				if (same_open == 0) {
					// Open
					this_menu.slideDown(300);
					this_select.addClass("selected");
					same_open = 1;
				} else {
					// Close
					this_menu.slideUp(300);
					this_select.removeClass("selected");
					same_open = 0;
				}
			} else if (current_menu.attr('id') != this_menu.attr('id')) {
				/* Clicking on a different menu */
				if (same_open == 1) {
					/* Close current menu, open this one */
					current_menu.slideUp(200);
					current_select.removeClass("selected");
					this_menu.delay(300).slideDown(300);
					this_select.addClass("selected");
				} else {
					/* Just open this menu */
					this_menu.slideDown(300);
					this_select.addClass("selected");
				}
				current_menu = this_menu;
				current_select = this_select;
				same_open = 1;
			}
		}
	});
});
